package ma.octo.assignement.exception.domain;

public class TransferNotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  public TransferNotFoundException() {
  }

  public TransferNotFoundException(String message) {
    super(message);
  }
}
