package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.domain.AccountNotFoundException;
import ma.octo.assignement.exception.domain.InsufficientBalanceException;
import ma.octo.assignement.exception.domain.TransactionException;
import ma.octo.assignement.exception.domain.TransferNotFoundException;
import ma.octo.assignement.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/transfers")
@Slf4j
@RequiredArgsConstructor
class TransferController {
    private final TransferService transferService;

    @GetMapping("/list")
    public ResponseEntity<List<TransferDto>> getAllTransfers() throws TransferNotFoundException {
        log.info("Retrieve list of all transfers");
        return new ResponseEntity<>(
                transferService.getAllTransfers(),
                HttpStatus.OK
        );
    }

    @PostMapping("/send")
    public void createTransfer(@RequestBody TransferDto transfer)
            throws AccountNotFoundException, TransactionException, InsufficientBalanceException {
        log.info("Create a new transfer");
        transferService.createTransfer(transfer);
    }
}
