package ma.octo.assignement.validator;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.TransferDto;

@Slf4j
public class TransferValidator {
    public static final int MAX_AMOUNT = 10_000;

    public static boolean isValid(TransferDto transfer) {
        if (transfer.getAmount() == null) {
            log.info("Amount is null");
            return false;
        } else if (transfer.getAmount().intValue() == 0) {
            log.info("Amount is 0");
            return false;
        } else if (transfer.getAmount().intValue() < 10) {
            log.info("Min account to send a transfer is 10");
            return false;
        } else if (transfer.getAmount().intValue() > MAX_AMOUNT) {
            log.info("Max amount to send a transfer is 10 000");
            return false;
        }

        if (transfer.getReason().length() < 0) {
            log.info("Reason is empty");
            return false;
        }

        return true;
    }
}
