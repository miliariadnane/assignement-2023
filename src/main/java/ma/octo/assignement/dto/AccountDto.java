package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.User;
import java.math.BigDecimal;

@Getter @Setter
public class AccountDto {
    private String accountNumber;
    private String rib;
    private BigDecimal balance;
    private User user;
}
