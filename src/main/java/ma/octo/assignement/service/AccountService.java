package ma.octo.assignement.service;

import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.exception.domain.AccountNotFoundException;

import java.util.List;

public interface AccountService {
    List<AccountDto> getAllAccounts() throws AccountNotFoundException;
}
