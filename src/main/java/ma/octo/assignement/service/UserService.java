package ma.octo.assignement.service;

import ma.octo.assignement.domain.User;
import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.exception.domain.UserNotFoundException;

import java.util.List;

public interface UserService {
    List<UserDto> getAllUsers() throws UserNotFoundException;
    User findUserByUsername(String username);
}
