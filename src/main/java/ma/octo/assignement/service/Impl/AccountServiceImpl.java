package ma.octo.assignement.service.Impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.exception.domain.AccountNotFoundException;
import ma.octo.assignement.mapper.AccountMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    @Override
    public List<AccountDto> getAllAccounts() throws AccountNotFoundException {
        log.info("Get all accounts");
        List<Account> accountList = accountRepository.findAll();
        if (accountList.isEmpty()) {
            log.info("No account found");
            throw new AccountNotFoundException("No account found");
        } else {
            return accountMapper.toListDto(accountList);
        }
    }
}
