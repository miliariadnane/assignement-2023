package ma.octo.assignement.service.Impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.domain.AccountNotFoundException;
import ma.octo.assignement.exception.domain.InsufficientBalanceException;
import ma.octo.assignement.exception.domain.TransactionException;
import ma.octo.assignement.exception.domain.TransferNotFoundException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import ma.octo.assignement.validator.TransferValidator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class TransferServiceImpl implements TransferService {
    private final TransferRepository transferRepository;
    private final TransferMapper transferMapper;
    private final AccountRepository accountRepository;
    private final AuditService auditService;

    @Override
    public List<TransferDto> getAllTransfers() throws TransferNotFoundException {

        log.info("Get all transfers");
        List<Transfer> transferList = transferRepository.findAll();
        if (transferList.isEmpty()) {
            log.info("No transfer found");
            throw new TransferNotFoundException("No transfer found");
        } else {
            return transferMapper.toListDto(transferList);
        }
    }

    @Override
    public void createTransfer(TransferDto transfer) throws AccountNotFoundException, TransactionException, InsufficientBalanceException {


        Account sender = accountRepository.findAccountByAccountNumber(transfer.getSenderAccountNumber())
                .orElseThrow(() -> new AccountNotFoundException("Sender account not found"));

        Account receiver = accountRepository.findAccountByAccountNumber(transfer.getReceiverAccountNumber())
                .orElseThrow(() -> new AccountNotFoundException("Receiver account not found"));

        if(!TransferValidator.isValid(transfer)) {
            throw new TransactionException("Invalid transfer");
        }

        if (sender.getBalance().intValue() - transfer.getAmount().intValue() < 0) {
            log.info("Insufficient balance");
            throw new InsufficientBalanceException("Insufficient balance for the user");
        }

        sender.setBalance(sender.getBalance().subtract(transfer.getAmount()));
        accountRepository.save(sender);

        receiver.setBalance(new BigDecimal(receiver.getBalance().intValue() + transfer.getAmount().intValue()));

        accountRepository.save(receiver);

        transfer.setReceiverAccountNumber(receiver.getAccountNumber());
        transfer.setSenderAccountNumber(sender.getAccountNumber());

        transferRepository.save(transferMapper.toEntity(transfer));

        auditService.auditTransfer("""
                Transfer from %s To %s
                Amount %s
                Reason %s
                """.formatted(transfer.getSenderAccountNumber(), transfer.getReceiverAccountNumber(),
                transfer.getAmount(), transfer.getReason()));
    }
}
