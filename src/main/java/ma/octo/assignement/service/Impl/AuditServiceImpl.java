package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.enumration.EventType;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AuditService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class AuditServiceImpl implements AuditService {
    private AuditRepository auditRepository;

    public void auditTransfer(String message) {
        log.info("Audit of the event {}", EventType.TRANSFER);
        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    public void auditDeposit(String message) {
        log.info("Audit of the event {}", EventType.DEPOSIT);
        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }
}
