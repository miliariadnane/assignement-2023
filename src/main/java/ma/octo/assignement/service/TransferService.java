package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exception.domain.AccountNotFoundException;
import ma.octo.assignement.exception.domain.InsufficientBalanceException;
import ma.octo.assignement.exception.domain.TransactionException;
import ma.octo.assignement.exception.domain.TransferNotFoundException;

import java.util.List;

public interface TransferService {
    List<TransferDto> getAllTransfers() throws TransferNotFoundException;
    void createTransfer(TransferDto transfer) throws AccountNotFoundException, TransactionException, InsufficientBalanceException;
}
