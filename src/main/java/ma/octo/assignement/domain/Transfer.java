package ma.octo.assignement.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "transfers")
@Data @AllArgsConstructor @NoArgsConstructor
public class Transfer implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(nullable = false, updatable = false)
  private Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal amount;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @ManyToOne
  private Account senderAccount;

  @ManyToOne
  private Account receiverAccount;

  @Column(length = 200)
  private String reason;
}
