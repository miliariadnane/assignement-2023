package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.enumration.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AuditServiceImplTest {
    @Mock
    private AuditRepository auditRepository;

    @InjectMocks
    private AuditServiceImpl auditService;

    @Test
    @DisplayName("Test auditTransfer success")
    void itShouldAuditTransferSuccessfully() {
        // Given
        String message = "Transfer audit message";

        // When
        auditService.auditTransfer(message);

        // Then
        verify(auditRepository).save(
                argThat(audit -> audit.getEventType() == EventType.TRANSFER && audit.getMessage().equals(message))
        );
    }

    @Test
    @DisplayName("Test auditDeposit success")
    void itShouldAuditDepositSuccessfully() {
        // Given
        String message = "Deposit audit message";

        // When
        auditService.auditDeposit(message);

        // Then
        verify(auditRepository).save(
                argThat(audit -> audit.getEventType() == EventType.DEPOSIT && audit.getMessage().equals(message))
        );
    }
}
