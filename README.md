## Responses :

- [x] 1. Domain and Dto classes : 
      * For the variable names, there's a mix between French and English :
        * It's better to use only one language. In this case, it's better to use english, because it's the language used 
        in the code and in service, controller and other others classes we already use english.
          * EventType ✅ Good
          * AuditDeposit ✅ Good
          * AuditTransfer ✅ Good
          * Compte -> Account
          * MoneyDeposit ✅ Good
          * Transfer ✅ Good
          * Utilisateur -> User
          
      * Serializable interface is not used for all classes. So implementing it is not necessary, but it's a good pratice
     which gives us the ability to serialize and deserialize objects that's mean more flexibility to use them in different contexts.
         
          > **Note :** In general classes for domains, we use nouns in singular

      * naming convention not respected (camelCase) for variables :
        * Account :
          * nrCompte -> accountNumber
          * solde -> balance
        * MoneyDeposit : 
          * Montant -> amount
          * nom_prenom_emetteur -> senderFullName
          * compteBeneficiaire -> receiverAccount
          * motifDeposit -> depositReason
        * Transfer :
          * montantTransfer -> transferAmount
          * compteEmetteur -> senderAccount
          * compteBeneficiaire -> receiverAccount
          * motifTransfer -> transferReason
        * EventType :
          * `DEPOSIT("Deposit d'argent")` -> `DEPOSIT("money deposit")`*
        * TransferDto :
          * nrCompteEmetteur -> senderAccountNumber
          * nrCompteBeneficiaire -> receiverAccountNumber
          * motif -> reason
          * montant -> amount
        * After fixing the naming convention, we need to generate new getters and setters for the classes.
      * Tables names are not good :
        * It's generally considered good practice to name the table in all lowercase and to separate the words with an underscore. Also the table name should be in plural.
          * `@Table(name = "UTILISATEUR")` -> `@Table(name = "users")`
          * `@Table(name = "audits")`
          * `@Table(name = "COMPTE")` -> `@Table(name = "accounts")`
          * `@Table(name = "TRAN")` -> `@Table(name = "transfers")`
          
- [x] 2. Exceptions packages
  * Change exception domains :
    * CompteNotFoundException -> AccountNotFoundException
    * SoldeDisponibleInsuffisantException -> InsufficientBalanceException
  * Change the structure of the package from to the following tree :
  ````tree
  │   ExceptionHandelingController.java
    │
    └───domain
    CompteNonExistantException.java
    HttpResponse.java
    SoldeDisponibleInsuffisantException.java
    TransactionException.java
  ````
  * Extends **ResponseEntityExceptionHandler** which is a class that provides centralized exception handling across all @RequestMapping methods through @ControllerAdvice classes.
  * Create a custom http response for handling exceptions. 
  * Change function names and structure for exception handling :
    * `@ExceptionHandler(CompteNonExistantException.class)` -> `@ExceptionHandler(AccountNotFoundException.class)`
    * `@ExceptionHandler(SoldeDisponibleInsuffisantException.class)` -> `@ExceptionHandler(InsufficientBalanceException.class)`
    * `@ExceptionHandler(TransactionException.class)` -> `@ExceptionHandler(TransactionException.class)`
  * Custom return object for exceptions :
    * `return createHttpErrorResponse();`

- [x] 3. Repositories
    * Rename the repositories :
      * `UtilisateurRepository` -> `UserRepository`
      * `CompteRepository` -> `AccountRepository`
    * Add `@Repository` annotation for each repository. It's optional but it's a good practice to use it.
    * Change the function "Account findByNrCompte(String nrCompte)" to "Optional<Account> findAccountByAccountNumber(String accountNumber);".
      * Optional here to avoid null pointer exception in case that the account number doesn't exist. 

- [x] 4. Service
   * Change **AuditService** from a class to an interface.
   * It's always to use interface for services and implements them in the service implementation class, to respect `Open/Closed` solid principle.
     * Open for extension and closed for modification.
     * Each time we need to add a new method to the service, we don't need to modify the service implementation class, we just need to add the method in the interface.
   * For the injection of the AuditTransferRepository using @Autowired annotation, it's better to use constructor injection instead of field injection.
     * Constructor injection is better because it allows the dependencies to be final and non-null.
     * In fact that in our project we are using lombok, so we use `@RequiredArgsConstructor` annotation to generate a constructor with all final fields.
   * "Audit de l'événement" -> "Audit of the event"

- [x] 5. Controller
    * It's better to use `@RestController` without specifying the path of the controller, because we can use the `@RequestMapping` annotation in the class to specify the path of the controller.
    * For controller path as goo practice we can use api version in the path, as follows :
      * `@RequestMapping("/api/v1/transfers")`
    * rename static variable **MONTANT_MAXIMAL** to **MAX_AMOUNT**
    * For better readability, we can replace : `10000` by `10_000`
    * Replace **LOGGER** by **@Slf4j** annotation, which is a lombok annotation that generates a logger field.
    * Replace `@Autowired` by `@RequiredArgsConstructor` annotation to inject the service using constructor injection.
    * Change path name for the controller :
      * `@GetMapping("/listeDesTransfers")` -> `@GetMapping("/list")`
    * Rename **loadAll()** to **getAllTransfers()**
    * Change return type of the function loadAllTransfers() from **List<Transfer>** to **ResponseEntity<List<Transfer>>
    * In the controller we are supposed to call the service layer, not the repository layer (repository layer is for the service layer). For that we need to :
      * create a service interface for the transfer service.
      * create a service implementation for the transfer service.
      * Returned class for the function **getAllTransfers()** should be **TransferDto** not **Transfer**.
      * Mapping from **Transfer** to **TransferDto** should be done in the service layer.
      * Change path in PostMapping from **executerTransfers** to **createTransfer**.
      * Migrate the logic of the function **createTransfer()** from the controller to the service layer.
      * Refactor the code of the function **createTransfer()**.
      * Create a validator class called **TransferValidator** to replace the if statement in the function **createTransfer()**.
    * For API that returns the list of all accounts should be in separate controller. To respect the single responsibility principle in SOLID.
      * create a new controller for the accounts.
      * create a new service interface for the accounts.
      * create a new service implementation for the accounts.
    * The same remark for users API.
  
- [x] 6. Main class "NiceBankApplication"

    * Migrate the logic of the function **run()** from the main class to another class in the config package called **DbSeeder**
    * Annotate the class with **@Configuration** annotation.
    * Annotate the function with **@Bean** annotation.
    * Execute the function **run()** in the main class using **run()** function of the **CommandLineRunner** interface.


- [x] 7. Security layer

    * For the security, I have used JWT with auth0 library.
    * I have a blog about how to implement JWT with Spring Boot using auth0 library, you can check it here : [link](https://www.miliari.me/blog/spring-security-jwt-auth0)
    * Documentation for auth0 library : [link](https://github.com/auth0/java-jwt) 

- [x] 8. Testing
   - [x] Mapper
   - [x] repository
   - [x] service
   - [x] Validator
   - [ ] controller (in progress)
   - [ ] integration test
